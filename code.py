import sys

prev_userInn = ''
prev_kktRegId = ''
opened = False
bad_user = False

for line in sys.stdin:
    line = line.strip()
    (userInn, kktRegId, time, subtype) = line.split('\t')

    if userInn != prev_userInn:
        bad_user = False
        opened = False
        prev_userInn = userInn

    if kktRegId != prev_kktRegId:
        opened = False
        prev_kktRegId = kktRegId

    if subtype == 'openShift':
        opened = True

    if subtype == 'closeShift':
        opened = False

    if subtype == 'receipt' and not opened and not bad_user:
        bad_user = True
        sys.stdout.write(userInn + '\n')
